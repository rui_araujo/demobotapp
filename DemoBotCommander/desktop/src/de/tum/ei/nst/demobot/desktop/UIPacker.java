package de.tum.ei.nst.demobot.desktop;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;;

public class UIPacker {

	public static void main(String[] args) {
        Settings settings = new Settings();
        settings.maxWidth = 2048;
        settings.maxHeight = 2048;
        TexturePacker.process(settings, "../DemoBotCommander-android/assets/data","../DemoBotCommander-android/assets/data", "textures");
	}
}
