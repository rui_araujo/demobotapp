package de.tum.ei.nst.demobot.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import de.tum.ei.nst.demobot.DemoBotCommander;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "DemoBotCommander";
		config.width = DemoBotCommander.WIDTH;
		config.height = DemoBotCommander.HEIGHT;
		new LwjglApplication(new DemoBotCommander(), config);
	}
}
