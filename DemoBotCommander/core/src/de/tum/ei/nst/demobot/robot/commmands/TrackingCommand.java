package de.tum.ei.nst.demobot.robot.commmands;

public class TrackingCommand implements RobotCommand {

	private final boolean enabled;
	private final int index;

	public TrackingCommand(boolean enabled, int index) {
		this.enabled = enabled;
		this.index = index;
	}

	@Override
	public String toCommandString() {
		return "!C" + (enabled ? index : "-") + '\n';
	}

}
