package de.tum.ei.nst.demobot.eDVS;

import com.badlogic.gdx.ai.Agent;
import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.PixmapTextureData;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;

import de.tum.ei.nst.demobot.robot.RobotTextParser;
import de.tum.ei.nst.demobot.tracking.TrackedPoint;

public class DVSCanvas extends BaseDrawable implements Drawable, Agent {

	private final static int WIDTH = 128;
	private final static int HEIGHT = 128;

	private boolean flipX;
	private boolean flipY;
	private boolean switchXandY;

	private final static float DECAY_PER_SECOND = 2.5f;
	private final static Color colorOn = new Color(1, 0, 0, 0.25f);
	private final static Color colorOff = new Color(0, 1, 0, 0.25f);
	private final Color[][] points;
	private final Array<DVSEvent> eventsList;

	private final PixmapTextureData pixmapTextureData;
	private final Texture pixmapTexture;
	private final Pixmap pixmap;
	private boolean showTrackedPoint;
	private TrackedPoint point;

	public DVSCanvas(Array<DVSEvent> eventsList, boolean showCOG) {
		points = new Color[WIDTH][HEIGHT];
		for (int i = 0; i < points.length; ++i)
			for (int j = 0; j < points[i].length; ++j)
				points[i][j] = new Color(Color.BLACK);
		setMinWidth(WIDTH);
		setMinHeight(HEIGHT);
		pixmap = new Pixmap(WIDTH, HEIGHT, Format.RGB565);
		pixmapTextureData = new PixmapTextureData(pixmap, Format.RGB565, false,
				false);
		pixmap.setColor(0);
		pixmap.fill();
		pixmapTexture = new Texture(pixmap);
		this.eventsList = eventsList;
		this.showTrackedPoint = showCOG;
		flipX = flipY = switchXandY = false;
		MessageDispatcher.getInstance().addListener(
				RobotTextParser.TRACKED_POINT_UPDATE, this);
	}

	@Override
	public void draw(Batch batch, float x, float y, float width, float height) {
		// pixmapTextureData is always updated on the act method
		if (showTrackedPoint) {
			if (point != null) {
				pixmap.setColor(Color.BLUE);
				pixmap.fillRectangle(point.x, point.y, 1, 1);
			}
		}
		pixmapTexture.load(pixmapTextureData);
		batch.draw(pixmapTexture, x, y, width, height);
	}

	// Color to be used on the update process
	private final Color decay = new Color();

	private long eventCount = 0;
	private double timeCount = 0;
	private double frameCount = 0;

	/**
	 * Updates the actor based on time. Typically this is called each frame by
	 * {@link Stage#act(float)}.
	 * <p>
	 * The default implementation calls {@link Action#act(float)} on each action
	 * and removes actions that are complete.
	 * 
	 * @param delta
	 *            Time in seconds since the last frame.
	 */
	public void act(float delta) {
		// updating events
		synchronized (eventsList) {
			eventCount += eventsList.size;
			for (DVSEvent event : eventsList) {
				if (event.x >= points.length || event.x < 0
						|| event.y >= points[event.x].length || event.y < 0) {
					System.err.println("Invalid event");
					continue;
				}
				// This is like this on purpose to match the user's expectation
				// on what is 'x' and what is 'y' being 'x' the axis along the
				// baseline
				if (flipX) {
					event.y = 127 - event.y;
				}
				if (flipY) {
					event.x = 127 - event.x;
				}
				if (switchXandY) {
					points[event.x][event.y].add(event.polarity ? colorOn
							: colorOff);
				} else {
					points[event.y][event.x].add(event.polarity ? colorOn
							: colorOff);
				}
			}
			DVSEvent.EVENT_POOL.freeAll(eventsList); // return event to the
														// event pool
			eventsList.clear();
		}
		// applying decay
		pixmap.setColor(255);
		pixmap.fill();
		decay.set(delta * DECAY_PER_SECOND, delta * DECAY_PER_SECOND, delta
				* DECAY_PER_SECOND, delta * DECAY_PER_SECOND);
		for (int i = 0; i < points.length; ++i) {
			for (int j = 0; j < points[i].length; ++j) {
				points[i][j].sub(decay);
				pixmap.setColor(points[i][j]);
				pixmap.fillRectangle(i, j, 1, 1);
			}
		}
		timeCount += delta;
		frameCount++;
		if (timeCount > 1.0 && eventCount > 0) {
			if (showTrackedPoint) {
				if (point != null) {
					System.out.println("Point x:" + point.y + " y:" + point.x);
				}
			}
			System.out.println("Events/s:" + ((double) eventCount / timeCount));
			System.out.println("FPS:" + frameCount);
			eventCount = 0;
			timeCount = 0;
			frameCount = 0;
		}
	}

	public void setShowTracking(boolean showCOG) {
		this.showTrackedPoint = showCOG;
	}

	public void toggleX() {
		flipX = !flipX;
	}

	public void toggleY() {
		flipY = !flipY;
	}

	public void toggleXandY() {
		switchXandY = !switchXandY;
	}

	public void answerAvailable(TrackedPoint point) {

	}

	@Override
	public void update(float delta) {
	}

	@Override
	public boolean handleMessage(Telegram msg) {
		if (msg.message == RobotTextParser.TRACKED_POINT_UPDATE) {
			this.point = (TrackedPoint) msg.extraInfo;
			return true;
		}
		return false;
	}

}
