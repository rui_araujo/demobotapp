package de.tum.ei.nst.demobot.robot;

import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Array;

import de.tum.ei.nst.demobot.Settings;
import de.tum.ei.nst.demobot.eDVS.DVSEvent;
import de.tum.ei.nst.demobot.robot.commmands.BiasCommand;
import de.tum.ei.nst.demobot.robot.commmands.EventsCommand;
import de.tum.ei.nst.demobot.robot.commmands.MotorCommand;
import de.tum.ei.nst.demobot.robot.commmands.MotorDriverCommand;
import de.tum.ei.nst.demobot.robot.commmands.RTCCommand;
import de.tum.ei.nst.demobot.robot.commmands.RecordCommand;
import de.tum.ei.nst.demobot.robot.commmands.RobotCommand;
import de.tum.ei.nst.demobot.robot.commmands.SensorCommand;
import de.tum.ei.nst.demobot.robot.commmands.SleepCommand;
import de.tum.ei.nst.demobot.robot.commmands.TimestampCommand;
import de.tum.ei.nst.demobot.robot.commmands.TrackingCommand;

public class RobotController implements
		RobotSocketManager.OnStatusChangedListener {
	private final Preferences prefs;

	private final BlockingQueue<String> queue;
	private final Array<DVSEvent> eventsList;
	private RobotSocketManager commandThread;
	private ConnectionState conectionState;
	private final HashMap<Integer, Integer> lastMotorCommand;
	private int lastBiasSettings;

	// Set to true when sending a sleep command, set to false by any other
	// command

	public enum ConnectionState {
		CONNECTED, CONNECTING, DISCONNECTED, SLEEPING
	};

	private final OnStatusChangedListerner listener;

	public interface OnStatusChangedListerner {
		public void onStatusChanged(ConnectionState state);
	}

	public RobotController(OnStatusChangedListerner listener, Preferences prefs) {
		queue = new ArrayBlockingQueue<String>(100);
		eventsList = new Array<DVSEvent>(10000);
		lastMotorCommand = new HashMap<Integer, Integer>(2);
		lastBiasSettings = -1;
		conectionState = ConnectionState.DISCONNECTED;
		this.listener = listener;
		this.prefs = prefs;
	}

	/**
	 * This method will be called from a different thread
	 */
	@Override
	public void onStatusChanged(boolean connected) {
		if (connected) {
			conectionState = ConnectionState.CONNECTED;
			updateListener();
		} else {
			conectionState = ConnectionState.DISCONNECTED;
			updateListener();
		}
	}

	public ConnectionState getConectionState() {
		return conectionState;
	}

	public void connect() {
		if (conectionState == ConnectionState.DISCONNECTED) {
			commandThread = new RobotSocketManager(queue, eventsList,
					prefs.getString(Settings.HOST_PREF),
					prefs.getInteger(Settings.PORT_PREF), this);
			conectionState = ConnectionState.CONNECTING;
			updateListener();
			commandThread.start();
		}

	}

	public void disconnect() {
		if (conectionState != ConnectionState.DISCONNECTED) {
			commandThread.dispose();
			conectionState = ConnectionState.DISCONNECTED;
			updateListener();
		}

	}

	public Array<DVSEvent> getEventList() {
		return eventsList;
	}

	private void updateListener() {
		listener.onStatusChanged(conectionState);
	}

	public void dispose() {
		if (commandThread != null)
			commandThread.dispose();
	}

	private void sendMotorCommand(MotorCommand com) {
		Integer lastSpeed = lastMotorCommand.get(com.getMotor());
		if (lastSpeed != null) {
			if (lastSpeed == com.getSpeed())
				return;
		}
		while (!queue.offer(com.toCommandString())) {
			queue.poll();
		}
		lastMotorCommand.put(com.getMotor(), com.getSpeed());
	}

	public String getCurrentSpeed(int motor) {
		Integer lastSpeed = lastMotorCommand.get(motor);
		if (lastSpeed == null)
			return "?";
		return lastSpeed.toString();
	}

	public void updateBiasSettings(int selection) {
		if (lastBiasSettings == selection)
			return;
		sendCommand(new BiasCommand(selection));
		lastBiasSettings = selection;
	}

	public void updateTimestampMode(int selection) {
		sendCommand(new TimestampCommand(selection));
		commandThread.updateTimestampMode(selection);
	}

	public void updateMotorsSpeed(int speedM1, int speedM2) {
		sendMotorCommand(MotorCommand.obtain(MotorCommand.MOTOR1, speedM1));
		sendMotorCommand(MotorCommand.obtain(MotorCommand.MOTOR2, speedM2));
	}

	public void stopMotor() {
		sendCommand(MotorCommand.obtain(MotorCommand.MOTOR1, 0));
		sendCommand(MotorCommand.obtain(MotorCommand.MOTOR2, 0));
	}

	public void sleep() {
		if (conectionState == ConnectionState.SLEEPING)
			return;
		final SleepCommand sleep = new SleepCommand();
		while (!queue.offer(sleep.toCommandString())) {
			queue.poll();
		}
		conectionState = ConnectionState.SLEEPING;
		updateListener();
	}

	public void disableEvents() {
		sendCommand(new EventsCommand(false));
	}

	public void enableEvents() {
		sendCommand(new EventsCommand(true));
	}

	public void updateRTC() {
		sendCommand(new RTCCommand());
	}

	public void startRecord() {
		sendCommand(new RecordCommand(true));
	}

	public void stopRecord() {
		sendCommand(new RecordCommand(false));
	}

	public void enableMotorDriver() {
		sendCommand(new MotorDriverCommand(true));
	}

	public void disableMotorDriver() {
		sendCommand(new MotorDriverCommand(false));
	}

	public void enableTracking() {
		sendCommand(new TrackingCommand(true, 0));
		sendCommand(new SensorCommand(false, 1 << SensorCommand.TRACKED_POINT));
	}

	public void enableTrackingFollower() {
		sendCommand(new TrackingCommand(true, 1));
		sendCommand(new SensorCommand(true, 1 << SensorCommand.TRACKED_POINT,
				200));
	}

	public void disableTracking() {
		sendCommand(new TrackingCommand(false, 0));
		sendCommand(new SensorCommand(false, 1 << SensorCommand.TRACKED_POINT));
	}

	private void sendCommand(RobotCommand com) {
		while (!queue.offer(com.toCommandString())) {
			queue.poll();
		}
	}

	@Override
	public void onAwaked() {
		if (conectionState == ConnectionState.SLEEPING) {
			conectionState = ConnectionState.CONNECTED;
			updateListener();
		}
	}

	@Override
	public boolean isSleeping() {
		return conectionState == ConnectionState.SLEEPING;
	}
}
