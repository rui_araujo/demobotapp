package de.tum.ei.nst.demobot.robot;

import java.util.Scanner;
import java.util.StringTokenizer;

import com.badlogic.gdx.ai.msg.MessageDispatcher;

import de.tum.ei.nst.demobot.tracking.TrackedPoint;

public class RobotTextParser {
	
	public static final int TRACKED_POINT_UPDATE = 0;
	
	public static void parseTextReply(String text) {
		if (text == null) {
			return;
		}
		StringTokenizer st = new StringTokenizer(text, "\n");
		while (st.hasMoreTokens()) {
			final String reply = st.nextToken();
			if (reply.startsWith("-S")) {
				// Sensor reply
				if (reply.startsWith("-S29")) {
					//TODO:improve later
					Scanner s = new Scanner(reply.substring(5));
					int x = s.nextInt();
					int y = s.nextInt();
					MessageDispatcher.getInstance().dispatchMessage(0, null, null, TRACKED_POINT_UPDATE, new TrackedPoint(x, y));
					s.close();
					continue;
				}
			}
			System.out.println(reply);
			// Otherwise we don't care
		}
	}
}
