package de.tum.ei.nst.demobot.tracking;



public class TrackedPoint {
	public int x;
	public int y;

	public TrackedPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
