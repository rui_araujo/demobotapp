package de.tum.ei.nst.demobot;

public class Settings {

	public final static int PORT_DEFAULT = 56000;
	public final static int MODE_DEFAULT = 0;
	public final static int TIMESTAMP_DEFAULT = 0;
	public final static int BIAS_DEFAULT_VALUE = 5;
	public final static String HOST_IP_DEFAULT = "10.162.177.41";
	public final static String HOST_PREF = "robot_host";
	public final static String PORT_PREF = "robot_port";

	public final static String MODE_PREF = "robot_mode";
	public final static String MANUAL_MODE = "Manual";
	public final static String LED_MODE = "LED Follower";
	public final static String LED_MODE_ROBOT = "LED Follower - Robot";

	public final static String BIAS_PREF = "bias_mode";
	public final static String BIAS_DEFAULT = "Default";
	public final static String BIAS_BRAGFOST = "BragFost";
	public final static String BIAS_FAST = "Fast";
	public final static String BIAS_STEREO_PAIR = "Stereo Pair";
	public final static String BIAS_MINI_DVS = "Mini DVS";
	public final static String BIAS_BRAGFOST_BALANCED = "BragFost - On/Off balanced";

	public final static String TIMESTAMP_PREF = "timestamp_mode";
	public final static String NO_TIMESTAMP = "No Timestamp";
	public final static String VARIABLE_TIMESTAMP = "Variable Timestamp";
	public final static String TWO_BYTE_TIMESTAMP = "2 Byte Timestamp ";
	public final static String THREE_BYTE_TIMESTAMP = "3 Byte Timestamp ";
	public final static String FOUR_BYTE_TIMESTAMP = "4 Byte Timestamp ";
	
	
	public final static String Y_INV_PREF = "y_inv";
	public final static String X_INV_PREF = "x_inv";
	public final static String X_Y_SWITCH_PREF = "x_y_switch";

	private Settings() {
	}

}
