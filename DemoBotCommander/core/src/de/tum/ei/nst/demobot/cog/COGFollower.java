package de.tum.ei.nst.demobot.cog;

import com.badlogic.gdx.graphics.Color;

public class COGFollower {

	private COGFollower() {
	}

	public static Center getCenterofGravity(Color[][] image, float threshold) {
		int area = 0;
		center.y = center.x = 0;
		for (int i = 0; i < image.length; ++i) {
			for (int j = 0; j < image[i].length; ++j) {
				if ((image[i][j].r > threshold) && (image[i][j].b > threshold)
						&& (image[i][j].g > threshold)) {
					area += 1;
					center.x += i;
					center.y += j;
				}
			}
		}
		if (area == 0)
			return null;
		center.x /= (float) area;
		center.y /= (float) area;
		return center;
	}

	private final static Center center = new Center();

	public static class Center {
		public int x;
		public int y;
	}
}
