package de.tum.ei.nst.demobot;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

import de.tum.ei.nst.demobot.screens.MainScreen;

public class DemoBotCommander extends Game {

	public final static int WIDTH = 1024;
	public final static int HEIGHT = 716;

	@Override
	public void create() {
		//TODO:Gdx.graphics.setContinuousRendering(false);
		setScreen(new MainScreen());
		Gdx.graphics.requestRendering();
	}

	@Override
	public void dispose() {
		super.dispose();
		if (getScreen() != null) {
			getScreen().dispose();
		}
	}
}
