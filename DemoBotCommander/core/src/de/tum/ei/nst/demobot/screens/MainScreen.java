package de.tum.ei.nst.demobot.screens;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;

import de.tum.ei.nst.demobot.DemoBotCommander;
import de.tum.ei.nst.demobot.Settings;
import de.tum.ei.nst.demobot.eDVS.DVSCanvas;
import de.tum.ei.nst.demobot.robot.RobotController;
import de.tum.ei.nst.demobot.robot.RobotController.ConnectionState;
import de.tum.ei.nst.demobot.robot.RobotController.OnStatusChangedListerner;
import de.tum.ei.nst.demobot.robot.commmands.MotorCommand;

public class MainScreen implements Screen, OnStatusChangedListerner {

	private final static float DVS_SIZE = 512;

	private final static float ACC_PER_SECOND = 15.0f;

	private final static int MANUAL_DISCONNECTED_STATE = 0x00;
	private final static int MANUAL_CONNECTED_STATE = 0x11;
	private final static int LED_DISCONNECTED_STATE = 0x20;
	private final static int LED_CONNECTED_STATE = 0x21;
	private final static int LED_ROBOT_DISCONNECTED_STATE = 0x30;
	private final static int LED_ROBOT_CONNECTED_STATE = 0x31;

	private int currentState;
	private int nextState;

	private final Preferences prefs;

	private final RobotController robotController;

	private final Stage stage;
	private Skin skin;
	private SpriteBatch batch;
	private TextureAtlas atlas;
	private Image backgroundRegion;
	private Table mainTable;

	private Table leftSide;
	private Button forwardArrow;
	private Button backArrow;
	private Label connectionState;
	private Label currentSpeedMotor1;
	private Label currentSpeedMotor2;

	private Table rightSideButtons;
	private TextButton showSettings;
	private TextButton toggleMotors;
	private TextButton recordButton;
	private TextButton sleepButton;
	private TextButton disconnectButton;

	private Dialog settingsDialog;
	private SelectBox<String> modeSelector;
	private SelectBox<String> timestampSelector;
	private SelectBox<String> biasSettingsSelector;

	private Dialog connectionDialog;

	private Image eDVSCanvas;
	private DVSCanvas dvs;
	private CheckBox flipX;
	private CheckBox flipY;
	private CheckBox switchXY;
	private String connectedIP;

	public MainScreen() {
		prefs = Gdx.app.getPreferences("robot");
		stage = new Stage(new ScalingViewport(Scaling.fit,
				DemoBotCommander.WIDTH, DemoBotCommander.HEIGHT));
		robotController = new RobotController(this, prefs);
	}

	@Override
	public void show() {
		skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
		atlas = new TextureAtlas(Gdx.files.internal("data/textures.atlas"));
		batch = new SpriteBatch();
		backgroundRegion = new Image(atlas.findRegion("metal_texture"));

		connectionState = new Label("Disconnected", skin);
		connectionState.setColor(Color.RED);
		currentSpeedMotor1 = new Label("", skin);
		currentSpeedMotor2 = new Label("", skin);

		stage.addActor(backgroundRegion);
		mainTable = new Table(skin);
		mainTable.setFillParent(true);
		stage.addActor(mainTable);

		leftSide = new Table(skin);
		leftSide.defaults().space(5);
		leftSide.add(connectionState).fillX().row();
		leftSide.add(currentSpeedMotor1).fillX().row();
		leftSide.add(currentSpeedMotor2).fillX().row();
		leftSide.pack();
		mainTable.addActor(leftSide);

		connectedIP = prefs.getString(Settings.HOST_PREF,
				Settings.HOST_IP_DEFAULT);
		final int modePref = prefs.getInteger(connectedIP,
				Settings.MODE_DEFAULT);
		switch (modePref) {
		case 0:
			nextState = currentState = MANUAL_DISCONNECTED_STATE;
			break;
		case 1:
			nextState = currentState = LED_DISCONNECTED_STATE;
			break;
		case 2:
			nextState = currentState = LED_ROBOT_DISCONNECTED_STATE;
			break;
		}
		initConnectionDialog(); // the order matters
		initSettingsDialog();
		initArrows();
		initEDVSCanvas();
		initRightSideButtons();

		stage.addListener(new InputListener() {
			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				if (manualControlsActivated()) {
					if (keycode == Keys.W) {
						if (goingBack) {
							goingBack = false;
							forwardArrow.setChecked(false);
							backArrow.setChecked(false);
						} else {
							forwardArrow.setChecked(true);
							goingForward = true;
						}
						return true;
					} else if (keycode == Keys.S) {
						if (goingForward) {
							goingForward = false;
							forwardArrow.setChecked(false);
							backArrow.setChecked(false);
						} else {
							backArrow.setChecked(true);
							goingBack = true;
						}
						return true;
					}
				}
				return false;
			}

			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				if (keycode == Keys.W) {
					goingForward = false;
					forwardArrow.setChecked(false);
					return true;
				} else if (keycode == Keys.S) {
					backArrow.setChecked(false);
					goingBack = false;
					return true;
				}
				return false;
			}
		});
		Gdx.input.setInputProcessor(stage);
		connectionDialog.show(stage);
	}

	@Override
	public void render(float delta) {
		// Clear the screen
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		currentSpeedMotor1.setText("Motor1:  "
				+ robotController.getCurrentSpeed(0));
		currentSpeedMotor2.setText("Motor2:  "
				+ robotController.getCurrentSpeed(1));
		if (currentState != nextState) {
			currentState = nextState;
			if (isConnected()) {
				toggleMotors.setVisible(true);
				recordButton.setVisible(true);
				disconnectButton.setVisible(true);
				sleepButton.setVisible(true);
				switch (currentState) {
				case LED_CONNECTED_STATE:
				case MANUAL_CONNECTED_STATE:
					forwardArrow.setVisible(true);
					backArrow.setVisible(true);
					dvs.setShowTracking(false);
					break;
				case LED_ROBOT_CONNECTED_STATE:
					forwardArrow.setVisible(false);
					backArrow.setVisible(false);
					dvs.setShowTracking(true);
					break;
				}
			} else {
				if (robotController.getConectionState() != ConnectionState.SLEEPING) {
					disconnectButton.setVisible(false);
				}
				toggleMotors.setVisible(false);
				recordButton.setVisible(false);
				sleepButton.setVisible(false);
				forwardArrow.setVisible(false);
				backArrow.setVisible(false);
			}
		}
		// Show the loading screen
		switch (robotController.getConectionState()) {
		case CONNECTED:
			connectionState.setColor(Color.GREEN);
			connectionState.setText("Connected");
			break;
		case CONNECTING:
			connectionState.setColor(Color.BLUE);
			connectionState.setText("Connecting");
			break;
		case SLEEPING:
			connectionState.setColor(Color.BLUE);
			connectionState.setText("Sleeping");
			break;
		case DISCONNECTED:
			connectionState.setColor(Color.RED);
			connectionState.setText("Disconnected");
			break;
		}
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
		if (manualControlsActivated()) {
			processInput(delta);
		}
	}

	private static float clampFloat(float speed, float max) {
		if (speed > max) {
			speed = max;
		} else if (speed < -max) {
			speed = -max;
		}
		return speed;
	}

	private float speed = 0;// Move this to its own model class TODO
	private boolean useAccel = false;

	public void processInput(float deltaTime) {
		// System.out.println((1/deltaTime));
		final ApplicationType appType = Gdx.app.getType();
		// should work also with
		// Gdx.input.isPeripheralAvailable(Peripheral.Accelerometer)
		boolean forward = true;
		boolean accelarating = true;

		float speedM1 = 0, speedM2 = 0, angle = 0;
		if (appType == ApplicationType.Android
				|| appType == ApplicationType.iOS) {

			// 1.54188*​e^​(0.278813*​x)-​1 for the roll processing
			// System.out.println("Roll " + Gdx.input.getRoll());
			// System.out.println("accellY " + Gdx.input.getAccelerometerY());
			// System.out.println("accellZ " + Gdx.input.getAccelerometerZ());
			if (useAccel) {
				float accel = Gdx.input.getAccelerometerZ();
				if (accel > 7) {
					accel -= 7;
				} else if (accel < 5) {
					accel -= 5;
				} else {
					accel = 0;
				}
				accel *= 10;
				if (accel != 0) {
					accel = clampFloat(accel, 30);
					System.out.println("Roll " + accel);
					// exponential fit {0,1},{11,40},{30,120}
					if (accel > 0) {
						accel = (float) Math.floor(13.9272f * Math
								.exp(0.072027f * accel)) - 13;
					} else {
						accel = (float) -Math.floor(13.9272f * Math
								.exp(0.072027f * -accel)) + 13;
					}
					speedM1 = speedM2 = clampFloat(accel,
							MotorCommand.MAX_SPEED);
					System.out.println("speedM1 "
							+ clampFloat(accel, MotorCommand.MAX_SPEED));
				}
			}
			/* Tilt processing */
			angle = clampFloat(-Gdx.input.getAccelerometerY(), 10);
			if (angle > 2 || angle < -2) {
				if (angle > 2) {
					angle -= 2;
				} else if (angle < -2) {
					angle += 2;
				}
			} else {
				angle = 0;
			}

		} else {
			if (Gdx.input.isKeyPressed(Keys.A))
				angle += 4f;
			if (Gdx.input.isKeyPressed(Keys.D))
				angle += -4f;

		}

		if (!useAccel) {
			if (goingForward)
				forward = true;
			else if (goingBack)
				forward = false;
			else
				accelarating = false;// When not going front or back, it keeps
										// the same speed

			speed = clampFloat(
					speed
							+ (accelarating ? ((forward ? deltaTime
									: -deltaTime) * ACC_PER_SECOND) : 0),
					MotorCommand.MAX_SPEED);
			speedM1 = speedM2 = (float) Math.floor(speed);
		}
		if (angle != 0) {
			// exponential fit {0,1},{3,40},{8,150}
			if (angle >= 0) {
				angle = (float) Math.floor(12.9016f * Math
						.exp(0.30717f * angle)) - 12;
			} else {
				angle = (float) -Math.floor(12.9016f * Math
						.exp(0.30717f * -angle)) + 12;
			}
			speedM1 -= angle;
			speedM2 += angle;
		}
		speedM1 = clampFloat(speedM1, MotorCommand.MAX_SPEED);
		speedM2 = clampFloat(speedM2, MotorCommand.MAX_SPEED);
		robotController.updateMotorsSpeed((int) speedM1, (int) speedM2);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
		// Place the loading bar at the same spot as the frame, adjusted a few
		// px
		mainTable.invalidateHierarchy();

		connectionDialog.setPosition(
				((stage.getWidth() - connectionDialog.getWidth()) / 2),
				((stage.getHeight() - connectionDialog.getHeight()) / 2));

		eDVSCanvas.setPosition((stage.getWidth() - eDVSCanvas.getWidth()) / 2,
				Math.round((stage.getHeight() - eDVSCanvas.getHeight()) / 2));

		rightSideButtons.setPosition(
				stage.getWidth() - rightSideButtons.getWidth() - 20 * width
						/ height, 40);

		leftSide.setPosition(eDVSCanvas.getX() - leftSide.getWidth(), 350);

		switchXY.setPosition(eDVSCanvas.getX(), 40);
		flipX.setPosition(switchXY.getX() + switchXY.getWidth() + 30, 40);
		flipY.setPosition(flipX.getX() + flipX.getWidth() + 30, 40);

	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
		goingForward = false;
		goingBack = false;
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		stage.dispose();
		skin.dispose();
		batch.dispose();
		atlas.dispose();
		robotController.dispose();
	}

	private boolean goingForward = false;
	private boolean goingBack = false;

	@Override
	public void onStatusChanged(final ConnectionState state) {
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				if (state == ConnectionState.CONNECTED) {
					robotController.disableEvents();
					robotController.stopMotor();
					robotController.enableMotorDriver();
					// robotController.updateRTC();
					robotController.updateTimestampMode(timestampSelector
							.getSelectedIndex());
					robotController.updateBiasSettings(biasSettingsSelector
							.getSelectedIndex());
					robotController.enableEvents();
					modeSelector.setSelectedIndex(prefs.getInteger(connectedIP,
							Settings.MODE_DEFAULT));
					switch (currentState) {
					case MANUAL_DISCONNECTED_STATE:
						robotController.disableTracking();
						nextState = MANUAL_CONNECTED_STATE;
						break;
					case LED_DISCONNECTED_STATE:
						robotController.enableTracking();
						nextState = LED_CONNECTED_STATE;
						break;
					case LED_ROBOT_DISCONNECTED_STATE:
						robotController.enableTrackingFollower();
						nextState = LED_ROBOT_CONNECTED_STATE;
						break;
					default:
						nextState = currentState;
						break;
					}
				} else {
					if (state == ConnectionState.DISCONNECTED) {
						connectionDialog.show(stage);
					}
					switch (currentState) {
					case MANUAL_CONNECTED_STATE:
						nextState = MANUAL_DISCONNECTED_STATE;
						break;
					case LED_CONNECTED_STATE:
						nextState = LED_DISCONNECTED_STATE;
						break;
					case LED_ROBOT_CONNECTED_STATE:
						nextState = LED_ROBOT_DISCONNECTED_STATE;
						break;
					default:
						nextState = currentState;
						break;
					}
				}
			}
		});
	}

	private boolean isConnected() {
		return (currentState & 0x00000001) != 0;

	}

	// Initialization code
	private void initConnectionDialog() {
		final int portPref = prefs.getInteger(Settings.PORT_PREF,
				Settings.PORT_DEFAULT);
		final BitmapFont f = skin.get(TextFieldStyle.class).font;
		final float size = Math.max(f.getBounds(connectedIP).width,
				f.getBounds(Integer.toString(portPref)).width) + 10;
		final TextField host = new TextField(connectedIP,
				skin.get(TextFieldStyle.class));
		final TextField port = new TextField(Integer.toString(portPref),
				skin.get(TextFieldStyle.class));
		connectionDialog = new Dialog(" No connection \n", skin) {
			protected void result(Object object) {
				if (object instanceof RobotController) {
					int portNumber = -1;
					try {
						portNumber = Integer.parseInt(port.getText());
					} catch (Exception e) {
						portNumber = -1;
						port.setText(Integer.toString(prefs.getInteger(
								Settings.PORT_PREF, portNumber)));
					}
					if (portNumber > 0) {
						connectedIP = host.getText();
						final int modePref = prefs.getInteger(connectedIP,
								Settings.MODE_DEFAULT);
						switch (modePref) {
						case 0:
							nextState = currentState = MANUAL_DISCONNECTED_STATE;
							break;
						case 1:
							nextState = currentState = LED_DISCONNECTED_STATE;
							break;
						case 2:
							nextState = currentState = LED_ROBOT_DISCONNECTED_STATE;
							break;
						}
						prefs.putString(Settings.HOST_PREF, connectedIP);
						prefs.putInteger(Settings.PORT_PREF, portNumber);
						prefs.flush(); // this accesses to the disk
					}
					robotController.connect();
				}
			}
		};
		connectionDialog.getContentTable().add("Host: ").expand().fillX()
				.padTop(20);
		connectionDialog.getContentTable().add(host).width(size).padTop(20)
				.row();
		connectionDialog.getContentTable().add("Port: ").expand().fillX();
		connectionDialog.getContentTable().add(port).width(size).row();
		connectionDialog.getContentTable().align(Align.left);
		connectionDialog.button(" Connect ", robotController);
	}

	private void initArrows() {
		forwardArrow = new Button(new TextureRegionDrawable(
				atlas.findRegion("forward_pedal")), new TextureRegionDrawable(
				atlas.findRegion("forward_pedal_pressed")),
				new TextureRegionDrawable(atlas
						.findRegion("forward_pedal_pressed")));
		backArrow = new Button(new TextureRegionDrawable(
				atlas.findRegion("back_pedal")), new TextureRegionDrawable(
				atlas.findRegion("back_pedal_pressed")),
				new TextureRegionDrawable(atlas
						.findRegion("back_pedal_pressed")));
		forwardArrow.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if (manualControlsActivated()) {
					if (goingBack) {
						goingBack = false;
						forwardArrow.setChecked(false);
						backArrow.setChecked(false);
					} else {
						forwardArrow.setChecked(true);
						goingForward = true;
					}
				}
				return true;
			}

			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				goingForward = false;
				goingBack = false;
				forwardArrow.setChecked(false);
			}
		});
		backArrow.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if (manualControlsActivated()) {
					if (goingForward) {
						goingForward = false;
						forwardArrow.setChecked(false);
						backArrow.setChecked(false);
					} else {
						backArrow.setChecked(true);
						goingBack = true;
					}
				}
				return true;
			}

			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				backArrow.setChecked(false);
				goingForward = false;
				goingBack = false;
			}
		});
		forwardArrow.setVisible(false);
		backArrow.setVisible(false);
		leftSide.add(forwardArrow).fillX().row();
		leftSide.add(backArrow).fillX().row();
	}

	private void initRightSideButtons() {
		toggleMotors = new TextButton("Disable\nMotors", skin);
		toggleMotors.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if (toggleMotors.getText().toString().equals("Disable\nMotors")) {
					toggleMotors.setText("Enable\nMotors");
					robotController.disableMotorDriver();
				} else {
					toggleMotors.setText("Disable\nMotors");
					robotController.enableMotorDriver();
				}
				return true;
			}
		});
		toggleMotors.setVisible(false);

		recordButton = new TextButton("Record", skin);
		recordButton.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if (recordButton.getText().toString().equals("Record")) {
					recordButton.setText("Stop");
					robotController.startRecord();
				} else {
					robotController.stopRecord();
					recordButton.setText("Record");
				}
				return true;
			}
		});
		recordButton.setVisible(false);

		sleepButton = new TextButton("Sleep", skin);
		sleepButton.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				robotController.sleep();
				return true;
			}
		});
		sleepButton.setVisible(false);

		disconnectButton = new TextButton("Disconnect", skin);
		disconnectButton.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				robotController.disconnect();
				return true;
			}
		});
		disconnectButton.setVisible(false);

		showSettings = new TextButton("Settings", skin);
		showSettings.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				settingsDialog.show(stage);
				return true;
			}
		});
		disconnectButton.pack();
		rightSideButtons = new Table(skin);
		rightSideButtons.defaults().space(20);
		rightSideButtons.add(toggleMotors).fillX().row();
		rightSideButtons.add(recordButton).fillX().row();
		rightSideButtons.add(sleepButton).fillX().row();
		rightSideButtons.add(disconnectButton).fillX().row();
		rightSideButtons.add(showSettings).fillX().row();
		rightSideButtons.pack();
		mainTable.addActor(rightSideButtons);
	}

	private void initSettingsDialog() {
		// The order of the selectors items matters, the selection position must
		// be in sync with the firmwares
		biasSettingsSelector = new SelectBox<>(skin);
		biasSettingsSelector.setItems(Settings.BIAS_DEFAULT,
				Settings.BIAS_BRAGFOST, Settings.BIAS_FAST,
				Settings.BIAS_STEREO_PAIR, Settings.BIAS_MINI_DVS,
				Settings.BIAS_BRAGFOST_BALANCED);
		final int biasPref = prefs.getInteger(Settings.BIAS_PREF,
				Settings.BIAS_DEFAULT_VALUE);
		biasSettingsSelector.setSelectedIndex(biasPref);
		modeSelector = new SelectBox<>(skin);
		final int modePref = prefs.getInteger(connectedIP,
				Settings.MODE_DEFAULT);
		modeSelector.setItems(Settings.MANUAL_MODE, Settings.LED_MODE,
				Settings.LED_MODE_ROBOT);
		modeSelector.setSelectedIndex(modePref);
		timestampSelector = new SelectBox<>(skin);
		timestampSelector.setItems(Settings.NO_TIMESTAMP,
				Settings.VARIABLE_TIMESTAMP, Settings.TWO_BYTE_TIMESTAMP,
				Settings.THREE_BYTE_TIMESTAMP, Settings.FOUR_BYTE_TIMESTAMP);
		final int timestampPref = prefs.getInteger(Settings.TIMESTAMP_PREF,
				Settings.TIMESTAMP_DEFAULT);
		timestampSelector.setSelectedIndex(timestampPref);

		settingsDialog = new Dialog(" Settings \n", skin) {
			protected void result(Object object) {
				Boolean result = (Boolean) object;
				if (!result)
					return;
				if (robotController.getConectionState() == ConnectionState.CONNECTED) {
					robotController.disableEvents();
					robotController.updateTimestampMode(timestampSelector
							.getSelectedIndex());
					robotController.updateBiasSettings(biasSettingsSelector
							.getSelectedIndex());
					robotController.enableEvents();
				}
				if (modeSelector.getSelected().equals(Settings.MANUAL_MODE)) {
					if (robotController.getConectionState() == ConnectionState.CONNECTED) {
						robotController.disableTracking();
						robotController.stopMotor();
						nextState = MANUAL_CONNECTED_STATE;
					} else {
						nextState = MANUAL_DISCONNECTED_STATE;
					}
				} else if (modeSelector.getSelected().equals(Settings.LED_MODE)) {
					if (robotController.getConectionState() == ConnectionState.CONNECTED) {
						robotController.enableTracking();
						robotController.stopMotor();
						nextState = LED_CONNECTED_STATE;
					} else {
						nextState = LED_DISCONNECTED_STATE;
					}
				} else if (modeSelector.getSelected().equals(
						Settings.LED_MODE_ROBOT)) {
					if (robotController.getConectionState() == ConnectionState.CONNECTED) {
						robotController.enableTrackingFollower();
						nextState = LED_ROBOT_CONNECTED_STATE;
					} else {
						nextState = LED_ROBOT_DISCONNECTED_STATE;
					}
				}
				// Update bias
				prefs.putInteger(Settings.BIAS_PREF,
						biasSettingsSelector.getSelectedIndex());
				prefs.putInteger(connectedIP, modeSelector.getSelectedIndex());
				prefs.flush(); // this accesses to the disk
			}
		};
		settingsDialog.getContentTable().add("Bias: ").expand().fillX()
				.padTop(20);
		settingsDialog.getContentTable().add(biasSettingsSelector).padTop(20)
				.row();
		settingsDialog.getContentTable().add("TimeStamp: ").expand().fillX();
		settingsDialog.getContentTable().add(timestampSelector).expand()
				.fillX().row();
		settingsDialog.getContentTable().add("Mode: ").expand().fillX();
		settingsDialog.getContentTable().add(modeSelector).expand().fillX()
				.row();
		settingsDialog.button(" Ok ", true);
		settingsDialog.button(" Cancel ", false);
	}

	private void initEDVSCanvas() {
		dvs = new DVSCanvas(robotController.getEventList(), false);
		eDVSCanvas = new Image(dvs) {
			@Override
			public void act(float delta) {
				super.act(delta);
				dvs.act(delta);// update the eDVS canvas
			}
		};
		eDVSCanvas.setWidth(DVS_SIZE);
		eDVSCanvas.setHeight(DVS_SIZE);
		// eDVSCanvas.scaleBy(DVS_SCALE, DVS_SCALE);
		flipX = new CheckBox("Flip X", skin);
		flipX.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				dvs.toggleX();
				return true;
			}
		});
		// flipX.getCells().get(0).size(flipX.getLabel().getHeight(),
		// flipX.getLabel().getHeight());
		flipY = new CheckBox("Flip Y", skin);
		flipY.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				dvs.toggleY();
				return true;
			}
		});
		switchXY = new CheckBox("X<=>Y", skin);
		switchXY.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				dvs.toggleXandY();
				return true;
			}
		});
		mainTable.addActor(eDVSCanvas);
		mainTable.addActor(switchXY);
		mainTable.addActor(flipX);
		mainTable.addActor(flipY);
	}

	private boolean manualControlsActivated() {
		return currentState == MANUAL_CONNECTED_STATE
				|| currentState == LED_CONNECTED_STATE;
	}
}
