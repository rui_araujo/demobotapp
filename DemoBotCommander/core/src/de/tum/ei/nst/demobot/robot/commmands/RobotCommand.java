package de.tum.ei.nst.demobot.robot.commmands;

public interface RobotCommand {
	public String toCommandString();
}
