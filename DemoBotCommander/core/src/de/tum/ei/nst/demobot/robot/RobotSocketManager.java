package de.tum.ei.nst.demobot.robot;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

import com.badlogic.gdx.utils.Array;

import de.tum.ei.nst.demobot.eDVS.DVSEvent;

public class RobotSocketManager extends Thread {
	private final BlockingQueue<String> queue;
	private final String hostIP;
	private final int port;
	private final OnStatusChangedListener listener;
	private RobotOutput robotOutput;
	private RobotInput robotInput;
	private final Array<DVSEvent> eventsList;
	private int timestampMode = 0;

	public interface OnStatusChangedListener {
		public void onStatusChanged(boolean connected);

		public void onAwaked();

		public boolean isSleeping();

	}

	public RobotSocketManager(BlockingQueue<String> queue,
			Array<DVSEvent> eventsList, String hostIP, int port,
			OnStatusChangedListener listener) {
		this.queue = queue;
		this.hostIP = hostIP;
		this.port = port;
		this.listener = listener;
		this.eventsList = eventsList;
	}

	public void updateTimestampMode(int selection) {
		timestampMode = selection;
		if (robotInput != null) {
			robotInput.currentProcessingStep = 0;
		}
	}

	@Override
	public void run() {
		Socket socket = null;
		try {
			socket = new Socket();
			socket.connect(new InetSocketAddress(hostIP, port), 5000);
			final PrintWriter output = new PrintWriter(
					socket.getOutputStream(), true);
			listener.onStatusChanged(true);
			robotOutput = new RobotOutput(queue, output);
			robotOutput.start();
			robotInput = new RobotInput(new BufferedInputStream(
					socket.getInputStream(), 5242880), listener);// big buffer
			robotInput.start();
			robotInput.join();
			if (!robotInput.stopRequested) {
				// Error with the socket
				robotOutput.stopRequested = true;
				robotOutput.interrupt();
				listener.onStatusChanged(false);
				return;
			}
			robotOutput.join();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		} finally {
			try {
				if (socket != null)
					socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			listener.onStatusChanged(false);
		}
	}

	public void dispose() {
		if (robotInput != null) {
			robotInput.stopRequested = true;
			robotInput.interrupt();
		}
		if (robotOutput != null) {
			robotOutput.stopRequested = true;
			robotOutput.interrupt();
		}
		try {
			join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static class RobotOutput extends Thread {
		private final BlockingQueue<String> queue;
		private final PrintWriter output;

		public RobotOutput(BlockingQueue<String> queue, PrintWriter output) {
			this.queue = queue;
			this.output = output;
		}

		private boolean stopRequested = false;

		@Override
		public void run() {
			while (!stopRequested) {
				try {
					final String command = queue.take();
					output.print(command);
					output.flush();
					System.out.println(command);
				} catch (InterruptedException e) {
				}
			}
			output.close();
			queue.clear();
		}

	}

	private class RobotInput extends Thread {

		private final InputStream inputstream;
		private final OnStatusChangedListener listener;
		private int currentProcessingStep = 0;
		private StringBuilder builder = new StringBuilder();
		private int eventX;
		private int eventY;
		private boolean eventPolarity;
		private long eventTimestamp;

		private final static int NO_TIMESTAMP = 0;
		private final static int VARIABLE_TIMESTAMP = 1;
		private final static int TWO_BYTE_TIMESTAMP = 2;
		private final static int THREE_BYTE_TIMESTAMP = 3;
		private final static int FOUR_BYTE_TIMESTAMP = 4;

		public RobotInput(InputStream inputstream,
				OnStatusChangedListener listener) {
			this.inputstream = inputstream;
			this.listener = listener;
		}

		private boolean stopRequested = false;

		private void addEvent() {
			synchronized (eventsList) {
				final DVSEvent event = DVSEvent.EVENT_POOL.obtain();
				event.set(eventX, eventY, eventPolarity, eventTimestamp);
				eventsList.add(event);
			}
		}

		@Override
		public void run() {
			try {
				// inputstream.read();
				while (!stopRequested) {
					int charReceived = inputstream.read();
					if (charReceived == -1)
						break;
					if (listener.isSleeping())
						listener.onAwaked();
					switch (currentProcessingStep) {
					case 0: {
						if ((charReceived & 0x80) == 0) {
							builder.append((char) charReceived);
						} else {
							if (builder.length() > 0) {
								RobotTextParser.parseTextReply(builder.toString());
								builder.setLength(0);
							}
							eventX = charReceived & 0x7f;
							currentProcessingStep++;
						}
						break;
					}
					case 1: {
						eventPolarity = ((charReceived & 0x80) != 0);
						eventY = charReceived & 0x7f;
						if (timestampMode == NO_TIMESTAMP) {
							currentProcessingStep = 0;
							eventTimestamp = System.currentTimeMillis();
							addEvent();
						} else {
							eventTimestamp = 0;
							currentProcessingStep++;
						}
						break;
					}
					case 2: {
						currentProcessingStep++;
						if (timestampMode == VARIABLE_TIMESTAMP) {
							eventTimestamp = charReceived & 0x7f;
							if ((charReceived & 0x80) != 0) {
								addEvent();
								currentProcessingStep = 0;
							}
						} else {
							eventTimestamp = charReceived;
						}
						break;
					}
					case 3: {
						if (timestampMode == VARIABLE_TIMESTAMP) {
							eventTimestamp = (eventTimestamp << 7)
									| (charReceived & 0x7f);
							if ((charReceived & 0x80) != 0) {
								addEvent();
								currentProcessingStep = 0;
							}
						} else {
							eventTimestamp = (eventTimestamp << 8)
									| (charReceived);
							if (timestampMode == TWO_BYTE_TIMESTAMP) {
								addEvent();
								currentProcessingStep = 0;
							} else {
								currentProcessingStep++;
							}
						}
						break;
					}
					case 4: {
						if (timestampMode == VARIABLE_TIMESTAMP) {
							eventTimestamp = (eventTimestamp << 7)
									| (charReceived & 0x7f);
							if ((charReceived & 0x80) != 0) {// If not set
																// discard event
								addEvent();
							}
							currentProcessingStep = 0;
						} else {
							eventTimestamp = (eventTimestamp << 8)
									| (charReceived);
							if (timestampMode == THREE_BYTE_TIMESTAMP) {
								addEvent();
								currentProcessingStep = 0;
							} else {
								currentProcessingStep++;
							}
						}
						break;
					}
					case 5: { //
						if (timestampMode == FOUR_BYTE_TIMESTAMP) {
							eventTimestamp = (eventTimestamp << 8)
									| (charReceived);
							addEvent();
							currentProcessingStep = 0;
						} else {
							throw new RuntimeException("Event parser bug");
						}
					}
					}

				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					inputstream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
