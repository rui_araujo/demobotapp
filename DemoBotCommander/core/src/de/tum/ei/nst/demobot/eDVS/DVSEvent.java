package de.tum.ei.nst.demobot.eDVS;

import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pool.Poolable;

public class DVSEvent implements Poolable {
	public int x = 0;
	public int y = 0;
	boolean polarity = false;
	public long timestamp = 0;

	public void set(int x, int y, boolean polarity, long timestamp) {
		this.x = x;
		this.y = y;
		this.polarity = polarity;
		this.timestamp = timestamp;
	}

	public final static Pool<DVSEvent> EVENT_POOL = new Pool<DVSEvent>(10000,
			Integer.MAX_VALUE) {
		protected DVSEvent newObject() {
			return new DVSEvent();
		}
	};

	@Override
	public void reset() {
		set(0, 0, true, 0);
	}
}
