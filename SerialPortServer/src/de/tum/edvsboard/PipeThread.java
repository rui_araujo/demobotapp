package de.tum.edvsboard;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import purejavacomm.SerialPort;
import de.tum.edvsboard.UARTUtils.PortAttribute;

public class PipeThread extends StoppableThread {
	private final String portName;
	private final PortAttribute baudRate;
	private SerialPort serialPort;
	private final ServerSocket serverSocket;

	private ServerOutput output;
	private ServerInput input;

	public PipeThread(ServerSocket serverSocket, String portname,
			PortAttribute baudRate) {
		this.serverSocket = serverSocket;
		this.portName = portname;
		this.baudRate = baudRate;
	}

	@Override
	public void run() {
		try {
			serialPort = UARTUtils.open(portName, baudRate);
			if (serialPort == null) {
				// TODO add listener
				stopRequested();
			}
			while (!stopRequested) {
				final Socket newConnection = serverSocket.accept();
				output = new ServerOutput(newConnection, serialPort);
				input = new ServerInput(newConnection, serialPort);
				output.start();
				input.start();
				output.join();
				input.join();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			try {
				serialPort.close();
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void stopRequested() {
		if (input != null)
			input.stopRequested();
		if (output != null)
			output.stopRequested();
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		super.stopRequested();
	}

	private static class ServerOutput extends StoppableThread {
		private final Socket socket;
		private final SerialPort serialPort;

		private ServerOutput(Socket socket, SerialPort serialPort) {
			this.socket = socket;
			this.serialPort = serialPort;
		}

		@Override
		public void run() {
			try (final InputStream inputstream = serialPort.getInputStream();
					final OutputStream out = socket.getOutputStream();) {
				System.out.println("new connection");
				final byte[] output = new byte[10240];
				while (!stopRequested) {
					int read = inputstream.read(output);
					if (read < 0)
						break;
					out.write(output, 0, read);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("output closed");

		}

		@Override
		public void stopRequested() {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			super.stopRequested();
		}
	}

	private static class ServerInput extends StoppableThread {
		private final Socket socket;
		private final SerialPort serialPort;

		private ServerInput(Socket socket, SerialPort serialPort) {
			this.socket = socket;
			this.serialPort = serialPort;
		}

		@Override
		public void run() {
			try (final InputStream inputStream = socket.getInputStream();
					final OutputStream out = serialPort.getOutputStream();) {
				final byte[] output = new byte[10240];
				while (!stopRequested) {
					int read = inputStream.read(output);
					if (read < 0)
						break;
					out.write(output, 0, read);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("input closed");

		}

		@Override
		public void stopRequested() {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			super.stopRequested();
		}
	}

}
