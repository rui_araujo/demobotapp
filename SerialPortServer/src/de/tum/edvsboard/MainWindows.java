package de.tum.edvsboard;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;

import de.tum.edvsboard.UARTUtils.PortAttribute;
import de.tum.edvsboard.UARTUtils.PortIdentifier;

public class MainWindows {
	private final JFrame frame;
	private final JLabel statusBar;
	private final JButton connectButton;
	private final JButton scanButton;
	private final JComboBox<PortIdentifier> uartComboBox;
	private final JComboBox<PortAttribute> baudRateComboBox;
	private final JTextField portTextField;

	private boolean hasServerStarted = false;
	private PipeThread pipeThread;

	private void changeUI(boolean newState) {
		uartComboBox.setEnabled(newState);
		baudRateComboBox.setEnabled(newState);
		portTextField.setEnabled(newState);
	}

	public MainWindows() {
		statusBar = new JLabel("Click Scan");
		connectButton = new JButton("Connect");
		connectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!hasServerStarted) {
					try {
						final String portName = (String) (((PortIdentifier) (uartComboBox.getSelectedItem())).getID());
						final ServerSocket serverSocket = new ServerSocket(Integer
								.parseInt(portTextField.getText()));
						pipeThread = new PipeThread( serverSocket, portName, (PortAttribute) baudRateComboBox.getSelectedItem());
						pipeThread.start();
						hasServerStarted = true;
						connectButton.setText("Disconnect");
						changeUI(false);
						scanButton.setEnabled(false);
					} catch (IOException e1) {
						e1.printStackTrace();
					}

				} else {
					pipeThread.stopRequested();
					connectButton.setText("Connect");
					connectButton.setEnabled(false);
					changeUI(false);
					scanButton.setEnabled(true);
					hasServerStarted = false;
				}
			}
		});

		scanButton = new JButton("Scan");
		scanButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				uartComboBox.removeAllItems();
				baudRateComboBox.removeAllItems();
				// add available COM ports to menu
				try {
					List<PortIdentifier> lid = UARTUtils
							.getPortIdentifierList();
					if (lid.size() == 0) {
						setMessage("No UART ports detected");
						changeUI(false);
						return;
					}
					for (PortIdentifier id : lid) {
						uartComboBox.addItem(id);
					}
					uartComboBox.setSelectedIndex(0);

					List<PortAttribute> lpa = UARTUtils.getPortAttributeList();
					for (PortAttribute pa : lpa) {
						baudRateComboBox.addItem(pa);
					}
					baudRateComboBox.setSelectedIndex(0);
					changeUI(true);
					connectButton.setEnabled(true);
				} catch (Error ex) {
					ex.printStackTrace();
					System.out
							.println("No UART library available - disabling UART ports");
					setErrorMessage("No UART ports detected");
					changeUI(false);
				}

			}
		});
		final JLabel uartComboBoxLabel = new JLabel("UART List:");
		uartComboBox = new JComboBox<PortIdentifier>();
		final JLabel baudRateComboBoxLabel = new JLabel("Baud rate:");
		baudRateComboBox = new JComboBox<PortAttribute>();
		final JLabel portTextFieldLabel = new JLabel("TCP Port:");

		// Allow only number in the port field
		final PlainDocument doc = new PlainDocument();
		doc.setDocumentFilter(new DocumentFilter() {
			@Override
			public void insertString(FilterBypass fb, int off, String str,
					AttributeSet attr) throws BadLocationException {
				fb.insertString(off, str.replaceAll("\\D++", ""), attr); // remove
																			// non-digits
			}

			@Override
			public void replace(FilterBypass fb, int off, int len, String str,
					AttributeSet attr) throws BadLocationException {
				fb.replace(off, len, str.replaceAll("\\D++", ""), attr); // remove
																			// non-digits
			}
		});
		portTextField = new JTextField();
		portTextField.setDocument(doc);
		portTextField.setText("56000");
		connectButton.setEnabled(false);
		changeUI(false);
		JPanel panel = new JPanel(new GridLayout(0, 2, 8, 2));
		panel.add(uartComboBoxLabel);
		panel.add(uartComboBox);
		panel.add(baudRateComboBoxLabel);
		panel.add(baudRateComboBox);
		panel.add(portTextFieldLabel);
		panel.add(portTextField);
		panel.add(scanButton);
		panel.add(connectButton);
		frame = new JFrame("Serial Port Server");
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		frame.getContentPane().add(statusBar, BorderLayout.SOUTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400, 150);
		frame.setLocationRelativeTo(null);
	}

	private void setMessage(String msg) {
		statusBar.setText(msg);
	}

	private void setErrorMessage(String msg) {
		statusBar.setText(msg);
	}

	private void setVisible(boolean visible) {
		frame.setVisible(visible);
	}

	public static void main(String[] args) {

		final MainWindows window = new MainWindows();
		window.setVisible(true);
	}

}
